from django import forms
from .models import Zkr

forms.DateInput.input_type="date"

class ZkrForm(forms.ModelForm):
    body = forms.CharField(label="", widget=forms.Textarea(attrs={
        'class' : 'bogdan-please',
        'id' : 'idishnik',
        'cols' : 45,
        'rows' : 8,
    }))
    date = forms.DateField(widget=forms.DateInput)
    boolean = forms.BooleanField()

    class Meta:
        model = Zkr
        fields = [
            'body',
            'date',
            'boolean',
        ]

    def clean_body(self):
        body = self.cleaned_data['body']
        num_words = len(body.split())
        if num_words < 4:
            raise forms.ValidationError("Not enough words!")
        elif num_words < 5:
            raise forms.ValidationError("At lest 5 words please")
        return body




class RawZkrForm(forms.Form):
    body = forms.CharField(widget=forms.Textarea(attrs={
        'class':'my-class',
    }))
    date = forms.DateField(widget=forms.DateInput)
    boolean = forms.BooleanField()