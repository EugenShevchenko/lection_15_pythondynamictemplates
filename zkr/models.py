from django.db import models

# Create your models here.
class Zkr(models.Model):
    body = models.TextField()
    date = models.DateField(auto_now=False)
    boolean = models.BooleanField(default='True')