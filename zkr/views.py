from django.shortcuts import render
from .models import Zkr
from pprint import pprint
# from .forms import RawZkrForm
from .forms import ZkrForm
from django.http import HttpResponse

# Create your views here.
# def shtototam_view(request):
#     obj = Zkr.objects.get(id=1)
#     all_obj = Zkr.objects.all()
#     context = {
#         "name" : "<h1>this is very nice Fall</h1>",
#         "number" : 10,
#         "messages": ['first'],
#         "boolean" : None,
#         "zkr" : obj,
#     }
#     return render(request, 'lection/lection16.html', context)

# def form_view(request):
#     context = {}
    
#     pprint(request.GET)
#     pprint(request.POST)
#     if request.method == 'POST':
#         my_title = request.POST.get('title')
#         Zkr.objects.create(body=my_title)
    
#     return render(request, '17/lection17.html', context)

# def form_view(request):
#     form = RawZkrForm()
#     if request.method == "POST":
#         form = RawZkrForm(request.POST)
#         if form.is_valid():
#             print(form.cleaned_data)
#             # Zkr.objects.Create(form.cleaned_data)

#             Zkr.objects.create(body=form.cleaned_data['body'],date=form.cleaned_data['date'],boolean=form.cleaned_data['boolean'])
#         else:
#             print(form.errors)
#     context = {
#         'form' : form,
#     }   
#     return render(request, '17/lection17.html', context)

# def myform_view(request):

#     my_form = ZkrForm()
#     if request.method == "POST":
#         my_form = ZkrForm(request.POST)
#         if my_form.is_valid():
#             # print(my_form.cleaned_data)
#             Zkr.objects.create(
#                 body=my_form.cleaned_data['body'],
#                 date=my_form.cleaned_data['date'],
#                 boolean=my_form.cleaned_data['boolean'])
#             my_form = ZkrForm()
#         else:
#             print(my_form.errors)
#     context = {
#         'form' : my_form,
#     }
#     return render(request, '17/zkr.html', context)

def myform_view(request):

    my_form = ZkrForm()
    if request.method == "POST":
        my_form = ZkrForm(request.POST)
        if my_form.is_valid():
            Zkr.objects.create(
                body = my_form.cleaned_data['body'],
                date = my_form.cleaned_data['date'],
                boolean = my_form.cleaned_data['boolean'],
            )
            my_form = ZkrForm()
            # print(my_form.cleaned_data)
        else:
            print(my_form.errors)
    context = {
        'form' : my_form,
    }
    return render(request, '17/zkr.html', context)















def search_view(request):
    context = {}
    return render(request, '18/search.html', context)

def search_handling(request):
    #BAD
    # message = 'You searched for: ' + request.GET['q']
    # return HttpResponse(message)

    #GOOD
    if 'q' in request.GET:
        message = 'You searched for: %r' % request.GET['q']
    else:
        message = 'You submitted an empty form.'
    return HttpResponse(message)