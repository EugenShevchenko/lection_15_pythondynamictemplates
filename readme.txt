start with showing PIP and tell them about dependencies managers

масиви та методи для роботи з масивами
цикли
функції
анонімні функції
класи та методи
це все десь пів пари

Коротше джанго переносимо на наступний тиждень

Dictionary function:
clear()	Removes all the elements from the dictionary
copy()	Returns a copy of the dictionary
fromkeys()	Returns a dictionary with the specified keys and values
get()	Returns the value of the specified key
items()	Returns a list containing the a tuple for each key value pair
keys()	Returns a list contianing the dictionary's keys
pop()	Removes the element with the specified key
popitem()	Removes the last inserted key-value pair
setdefault()	Returns the value of the specified key. If the key does not exist: insert the key, with the specified value
update()	Updates the dictionary with the specified key-value pairs
values()	Returns a list of all the values in the dictionary

# IF ELSE
a = 200
b = 33
if b > a:
  print("b is greater than a")
elif a == b:
  print("a and b are equal")
else:
  print("a is greater than b")
# and or

# while LOOP
i = 1
while i < 6:
  print(i)
  if i == 3:
    break
  i += 1

# FOR LOOPD not print banana:

fruits = ["apple", "banana", "cherry"]
for x in fruits:
  if x == "banana":
    continue
  print(x)

# MATRIX
adj = ["red", "big", "tasty"]
fruits = ["apple", "banana", "cherry"]

for x in adj:
  for y in fruits:
    print(x, y)

# Classes
class Person:
  def __init__(self, name, age):
    self.name = name
    self.age = age

  def myfunc(self):
    print("Hello my name is " + self.name)

p1 = Person("John", 36)
p1.myfunc()

# Modules import
show

# Dates
import datetime

x = datetime.datetime.now()
print(x)
print(x.strftime("%B"))
# formatting dates

%a	Weekday, short version	Wed	
%A	Weekday, full version	Wednesday	
%w	Weekday as a number 0-6, 0 is Sunday	3	
%d	Day of month 01-31	31	
%b	Month name, short version	Dec	
%B	Month name, full version	December	
%m	Month as a number 01-12	12	
%y	Year, short version, without century	18	
%Y	Year, full version	2018	
%H	Hour 00-23	17	
%I	Hour 00-12	05	
%p	AM/PM	PM	
%M	Minute 00-59	41	
%S	Second 00-59	08	
%f	Microsecond 000000-999999	548513	
%z	UTC offset	+0100	
%Z	Timezone	CST	
%j	Day number of year 001-366	365	
%U	Week number of year, Sunday as the first day of week, 00-53	52	
%W	Week number of year, Monday as the first day of week, 00-53	52	
%c	Local version of date and time	Mon Dec 31 17:41:00 2018	
%x	Local version of date	12/31/18	
%X	Local version of time	17:41:00	
%%	A % character	%

# JSON - import JSON and tell them about it
import json


Встановлення джанго
Встановити піп
https://pip.pypa.io/en/latest/installing/
curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
python get-pip.py

https://docs.djangoproject.com/en/2.1/howto/windows/


ok we have installed django, what's now?

workon lection13
pip install mysql-connector


Monday Lection:

mkvirtualenv myproject
cd myproject
pip install django

workon myproject
deactivate

in your project create \src folder
django-admin startproject myproject

python manage.py runserver

settings.py
BaseDir = ваша базова директорія, де саме знаходиться ваш пайтон проект:
print(BaseDir)

secret key
debug
search what is allowed hosts[]
installed apps = 

python manage.py migrate - Запуск міграції, по дефолту це буде sqlite
python manage.py createsuperuser

Tell them about MVC concept - Model View Controller

Python does NOT have private methods, so yes you can overwrite everything,
convention is to use __methodname, to saym don't touch it it's private
public
private
protected

create your own app

python manage.py starapp cta
include your app in settings.py in section INSTALLED_APPS
in migration file of your app create class inherited from Model with several text fields

# Create your models here.
class Cta(models.Model):
    title = models.TextField()
    image_src = models.TextField()
    alt = models.TextField(default='image alt')

List of all DB fields
https://www.webforefront.com/django/modeldatatypesandvalidation.html

python manage.py makemigrations
python manage.py migrate

in your app admin.py
add this lines, this will allow you to fill in database fields
# Register your models here.
from .models import Cta

admin.site.register(Cta)


postgreSQL config:
pip install psycopg2

in settings.py

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'db_name',                      
        'USER': 'db_user',
        'PASSWORD': 'db_user_password',
        'HOST': '',
        'PORT': 'db_port_number',
    }
}


process all possible fields from documentation
try to connect to postgreSQL database
inheritance of class

в ідеалі ще додати сюди вьюху та темплейт


Завдання на практичну
Підключити галп в пайтон
Підключити власний темплейт в пайтон
Вивести дані на свій темплейт зі свого пайтону

В ідеалі підключити блоки і створити декілька сторінок на своєму проекті


views.py file from our application
#tutorial 12-13
def home_view(request, *args, **kwargs):
    print(request.user)
    return HttpResponse("<h1>AAA</h1>")

New lection:
1) Install and start Django once again
2) Connect database and start own application
3) Fill own db table, overview docs and play around with different data types
4) Work with views
in your app define function for in views.py file

from django.http import HttpResponse
def home_view(*args, **kwargs):
    return HttpResponse("<h1>Homepage</h1>")

in urls.py
follow instructions in comments
or
from cta.views import home_view, contact_view
urlpatterns = [
    path('', home_view, name = 'home'),
    path('admin/', admin.site.urls),
]

Play around with different url's

def home_view(request, *args, **kwargs):
    print(request.user)
    return HttpResponse("<h1>Homepage</h1>")


5) django html template
create templates directory in ROOT, and put html file there
in setting.py tempates DIRS
'DIRS': [
    os.path.join(BASE_DIR, 'templates'),
],
in views return this string
return render(request, 'home.html', {})

Dynamic html templates

<h1>Hello, {{request.user}}</h1>


views.py

from django.shortcuts import render
from django.http import HttpResponse
from .models import Cta

# Create your views here.
def home_view(request, *args, **kwargs):
    first = Cta.objects.get(pk=1)
    all_entries = Cta.objects.all()

    context = {
        "first_cta": first,
        "all" : all_entries,
    }
    return render(request, 'home.html', context)

base.html

<!doctype html>
<html>
    <head>
        <title>Django</title>
    </head>
    <body>
        {% include 'navbar.html' %}
        
        {% block content %}
        {% endblock %}
    </body>
</html>

home.html

{% extends 'base.html' %}
{% block content %}
<h1>Hello, {{request.user}} </h1>
<h3>{{ first_cta.title }}</h3>
<ul>
    {% for item in all %}
        <li> {{ item.title }} </li>
    {% endfor %}
</ul>
{% endblock %}




6) include css files
7) Include gulp




install npm
npm init
npm install gulp --save-dev
npm install gulp-watch --save-dev

create gulpfile.js

create all necessary folders
static/css
src/scss

include in settings.py
STATICFILES_DIRS = [
    os.path.join(BASE_DIR, "static"),
]

install 
pip install django-gulp
https://pypi.org/project/django-gulp/

edit gulpfile.js
try runserver with gulp default task
