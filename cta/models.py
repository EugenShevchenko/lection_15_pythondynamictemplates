from django.db import models

# Create your models here.
class Cta(models.Model):
    title = models.TextField()
    image_src = models.TextField()
    alt = models.TextField(default='image alt')
    active = models.BooleanField(default='True')
    date = models.DateField(auto_now=True)
