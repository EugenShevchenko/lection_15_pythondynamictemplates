from django.shortcuts import render
from django.http import HttpResponse

from .models import Cta

# Create your views here.

def home_view(*args, **kwargs):
    return HttpResponse("<h1>Bogdan</h1>")

def kwarg_view(*args, **kwargs):
    return HttpResponse("<h2>Python rules</h2>")

def template_view(request, *args, **kwargs):
    first = Cta.objects.get(pk=1)
    all_cta = Cta.objects.all()
    context = {
        "name": "Vasiliy",
        "lastname" : "Pupkin<script>alert('AAA');</script>",
        "cta" : first,
        "all" : all_cta,
        "messages" : {'first', 'second'},
        "boolean" : False,
    }
    return render(request, 'home.html', context)


    