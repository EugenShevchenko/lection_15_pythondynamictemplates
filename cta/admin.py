from django.contrib import admin

# Register your models here.
from .models import Cta

admin.site.register(Cta)