# Generated by Django 2.1.2 on 2018-10-30 07:04

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cta', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='cta',
            name='active',
            field=models.BooleanField(default='True'),
        ),
        migrations.AddField(
            model_name='cta',
            name='date',
            field=models.DateField(auto_now=True),
        ),
    ]
