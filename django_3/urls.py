"""django_3 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from cta import views
from zkr import views as zkr


urlpatterns = [
    path('', views.template_view, name='home'),
    # path('lection', zkr.shtototam_view),
    # path('form', zkr.form_view),
    path('form', zkr.myform_view),
    path('search-form', zkr.search_view),
    path('search/', zkr.search_handling),
    path('about', views.home_view),
    path('admin/', admin.site.urls),
]
